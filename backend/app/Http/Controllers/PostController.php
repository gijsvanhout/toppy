<?php

namespace App\Http\Controllers;

use App\Http\Requests\Api\Post\StoreRequest;
use App\Http\Requests\Api\Post\UpdateRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return PostResource::collection(Post::query()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $post = Post::create($request->validated());
        return PostResource::make($post);
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        return PostResource::make($post);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Post $post)
    {
        $post->update($request->validated());
        return PostResource::make($post);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $post->delete();
    
        return Response()->noContent();
    }

    /**
     * Enable the specified resource.
     */
    public function enable(Post $post) {
        $post->enabled = true;
        $post->save();
        return PostResource::make($post);
    }

    /**
     * Disable the specified resource.
     */
    public function disable(Post $post) {
        $post->enabled = false;
        $post->save();
        return PostResource::make($post);
    }
}
