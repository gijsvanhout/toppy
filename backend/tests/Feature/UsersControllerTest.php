<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UsersControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_list_users(): void
    {
        //arrange
        $loggedInUser = User::factory()->create();
        User::factory()->count(50)->create();

        //act
        $response = $this->actingAs($loggedInUser)->getJson("/api/users");

        //assert
        $response->assertJsonCount(15, "data");
    }
}
