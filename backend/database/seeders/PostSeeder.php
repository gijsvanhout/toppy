<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $posts = [
            [
                'title' => 'Leuke dingen doen',
                'content' => 'Word jij onze nieuwe logistiek topper? Of kom je ons
                kantoor versterken als één van onze development specialisten? Of help je
                onze klanten met leuke dingen doen? Vind nu je droombaan hier bij
                Toppy!',
                'slug' => 'leuke-dingen-doen"',
                'enabled' => true,
            ],
        ];

        foreach ($posts as $post) {
            \App\Models\Post::create($post);
        }
    }
}
