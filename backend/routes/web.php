<?php

use App\Http\Controllers\PostController;
use Illuminate\Foundation\Http\Middleware\ValidateCsrfToken;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'api'], function() {
    Route::apiResource("posts", PostController::class);
    Route::patch('posts/{post}/enable', [PostController::class, 'enable'])->name('posts.enable');
    Route::patch('posts/{post}/disable', [PostController::class, 'disable'])->name('posts.disable');
})->withoutMiddleware(ValidateCsrfToken::class);