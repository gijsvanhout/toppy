# Posts API in Docker

## Description
This API allows users to create, read, update, and delete posts. It can be run using Docker.

## Table of Contents
- [Posts API in Docker](#posts-api-in-docker)
  - [Description](#description)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
  - [Usage](#usage)
  - [Endpoints](#endpoints)
  - [Authentication](#authentication)

## Installation
1. Clone the repository: `git clone https://gitlab.com/gijsvanhout/toppy`
2. Change to the project directory
3. Run Docker Compose: `docker-compose up -d`

## Usage
Access the API at `http://localhost/api`

## Endpoints
- `GET api/posts`: Get all posts
- `GET api/posts/:id`: Get a specific post by ID
- `POST api/posts`: Create a new post
- `PUT api/posts/:id`: Update a post by ID
- `DELETE api/posts/:id`: Delete a post by ID

## Authentication
This API does not require authentication.