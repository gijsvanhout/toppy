#!/bin/bash
# start.sh

composer install

# Wacht tot MySQL beschikbaar is
attempt=1
max_attempts=10
while ! mysqladmin ping -h"mysql" --silent; do
    echo "Wachten op dat MySQL beschikbaar wordt (poging: $attempt)"
    attempt=$((attempt+1))
    if [ $attempt -gt $max_attempts ]; then
      echo "Maximale pogingen bereikt, kan niet verbinden met MySQL"
      exit 1
    fi
    sleep 5
done

echo "MySQL is beschikbaar, uitvoeren van migraties..."
php artisan key:generate
php artisan migrate --force --seed
php artisan config:cache

echo "Migraties voltooid, starten van de main process..."
exec "$@"
